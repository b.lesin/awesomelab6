<?php
/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW'])) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="For lab6"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}
include 'db_info.php';

$request = "SELECT * from admin";
$result = $db->prepare($request);
$result->execute();
$flag=0;
while($data=$result->fetch()){
    if($data['login']==$_SERVER['PHP_AUTH_USER'] && password_verify($_SERVER['PHP_AUTH_PW'],$data['pass'])){
        $flag=1;
    }
}
if($flag==0){
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="For lab6"');
    print('<h1>401 Требуется авторизация</h1>');
    exit();
}
print('Вы успешно авторизовались и видите защищенные паролем данные.');
print '<div>Привет админ!</div>';
print '<br/>';
// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********

$request = "SELECT * from form2";
$result = $db ->prepare($request);
$result->execute();
?>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Админка для 6 лабы</title>
    <link rel="stylesheet" media="all" href="style.css"/>

</head>
<body>
<form action="delete.php" method="post" accept-charset="UTF-8">
    <label>
        <input type="number" name="del_by_id">
    </label>
    <input type="submit" style="margin-bottom : -1em"  class="buttonform" value="Удалить запись по ID">
</form>
<form action="change.php" method="post" accept-charset="UTF-8">
    <label>
        <input type="number" name="chg_by_id">
    </label>
    <input type="submit" style="margin-bottom : -1em"  class="buttonform" value="Изменить запись по ID">
</form>
<?php

$request = "SELECT * from form2 INNER JOIN abil2 on form2.id=abil2.id";
$result = $db ->prepare($request);
$result->execute();
print '<table class="table">';
print '<tr><th>ID</th><th>Имя</th><th>E-Mail</th><th>Дата рождения</th><th>Пол</th><th>Кол-во конечностей</th>
<th>Биография</th><th>Логин</th><th>Хэш пароля</th><th>Бессмертие</th><th>Прохождение сквозь стены</th><th>Левитация</th></tr>';
while($data = $result->fetch()){
    print '<tr><td>';
    print $data['id'];
    print '</td><td>';
    print $data['name'];
    print '</td><td>';
    print $data['email'];
    print '</td><td>';
    print $data['year'];
    print '</td><td>';
    print $data['sex'];
    print '</td><td>';
    print $data['lb'];
    print '</td><td>';
    print $data['bio'];
    print '</td><td>';
    print $data['login'];
    print '</td><td>';
    print $data['pass'];
    print '</td><td>';
    print $data['immortal'];
    print '</td><td>';
    print $data['phasing'];
    print '</td><td>';
    print $data['levitation'];
    print '</td></tr>';
}
print '</table>';

$request = "SELECT COUNT(immortal) FROM abil2 WHERE immortal=1 GROUP BY immortal ";
$result = $db ->prepare($request);
$result->execute();
$data_im = $result->fetch()[0];
$request = "SELECT COUNT(phasing) FROM abil2 WHERE phasing=1 GROUP BY phasing ";
$result = $db ->prepare($request);
$result->execute();
$data_ph = $result->fetch()[0];
$request = "SELECT COUNT(levitation) FROM abil2 WHERE levitation=1 GROUP BY levitation ";
$result = $db ->prepare($request);
$result->execute();
$data_lv = $result->fetch()[0];
print '<h2>Статистика по сверхспособностям:</h2>';
print '<table class="table">';
print '<tr><th>Бессмертие</th><th>Прохождение сквозь стены</th><th>Левитация</th></tr>';
print '<tr><td>';
print $data_im;
print '</td><td>';
print $data_ph;
print '</td><td>';
print $data_lv;
print '</td></tr>';
print '</table>';


?>


</body>
